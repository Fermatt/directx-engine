#include "StdAfx.h"
#include "PlayState.h"
#include "Transform.h"
#include "TileCreator.h"
#include "Renderer.h"
#include "LoadModel.h"
#include "SpotLightSetter.h"
#include "BoundingBox.h"

PlayState::PlayState(void)
{
}


PlayState::~PlayState(void)
{
	delete mod;
	delete cam;
	for (int i = 0; i < obj.size(); i++)
	{
		delete obj.back();
		obj.pop_back();
	}
}

void PlayState::create()
{
	mod = new Model(dev);
	cam = new Camara();

	//MainCam = cam;

	Test = 7;

	effect = NULL;

	if (Test == 0)
	{
		Texture* tex1 = new Texture(L"../Assets/metal.jpg", dev);
		Texture* tex2 = new Texture(L"../Assets/water.jpg", dev);
		std::vector<std::vector<std::vector<int>>> tmint = { { { 0,-1,0 },{ -1,-1,-1 },{ 0,0,0 } },{ { -1,-1,-1 },{ 1,1,1 },{ -1,-1,-1 } } };
		std::vector<Texture*> tmtex = { tex1, tex2 };
		com.push_back(new Composite());
		Component* tl = new TileCreator(tmint, tmtex, 1, 1, mod->getSquare());
		Transform* tr = new Transform();
		com[com.size() - 1]->Add(tr);
		com[com.size() - 1]->Add(tl);
		com[com.size() - 1]->SetParent(NULL);
	}
	else if (Test == 1)
	{
		com.push_back(new Player(dev, cam, false));
		com[0]->GetComponent<Transform>()->move(D3DXVECTOR3(0, 0, 5));
	}
	else if (Test == 2)
	{
		com.push_back(new Player(dev, cam, false));
		com.push_back(new Player(dev, cam, false));
		com[1]->GetComponent<Transform>()->move(D3DXVECTOR3(2, 0, 0));
		com[1]->SetParent(com[0]);
		com[0]->GetComponent<Transform>()->move(D3DXVECTOR3(0, 0, 5));
	}
	else if (Test == 3)
	{
		D3DXCreateEffectFromFile(
			dev, L"../Engine/shaderTexTinte.fx", NULL, NULL,
			D3DXSHADER_ENABLE_BACKWARDS_COMPATIBILITY,
			NULL, &effect, NULL);
		com.push_back(new Composite());
		BoundingBox* bb;
		LoadModel* mod2 = new LoadModel("../Assets/bus.obj", dev, bb);
		Component* tl = new Renderer(mod2->getModel(), dev, cam);
		tl->bb = bb;
		com[com.size() - 1]->Add(tl);
		Transform* tr = new Transform();
		tr->move(D3DXVECTOR3(0, 0, 10));
		com[com.size() - 1]->Add(tr);
		com[com.size() - 1]->SetParent(NULL);

	}
	else if (Test == 4)
	{
		D3DXCreateEffectFromFile(
			dev, L"../Engine/shaderTexTinte.fx", NULL, NULL,
			D3DXSHADER_ENABLE_BACKWARDS_COMPATIBILITY,
			NULL, &effect, NULL);
		com.push_back(new Player(dev, cam, true));
		com[0]->GetComponent<Transform>()->move(D3DXVECTOR3(0, 0, 5));
	}
	else if (Test == 5)
	{
		D3DXCreateEffectFromFile(
			dev, L"../Engine/shaderDLight.fx", NULL, NULL,
			D3DXSHADER_ENABLE_BACKWARDS_COMPATIBILITY,
			NULL, &effect, NULL);
		ls = new LightSetter();
		com.push_back(new Player(dev, cam, true));
		com[0]->GetComponent<Transform>()->move(D3DXVECTOR3(0, 0, 2));
	}
	else if (Test == 6)
	{
		D3DXCreateEffectFromFile(
			dev, L"../Engine/shaderSpotLight.fx", NULL, NULL,
			D3DXSHADER_ENABLE_BACKWARDS_COMPATIBILITY,
			NULL, &effect, NULL);
		sls = new SpotLightSetter(0);
		sls2 = new SpotLightSetter(180);
		com.push_back(new Player(dev, cam, true));
		com[0]->GetComponent<Transform>()->move(D3DXVECTOR3(0, 0, 2));
		com[0]->GetComponent<Transform>()->scale(D3DXVECTOR3(2, 2, 1));
	}
	else if (Test == 7)
	{
		com.push_back(new Composite());
		BoundingBox* bb = new BoundingBox();
		LoadModel* mod2 = new LoadModel("../Assets/bus.obj", dev, bb);
		Component* tl = new Renderer(mod2->getModel(), dev, cam);
		tl->bb->Combine(bb);
		tl->bb->Refresh();
		com[com.size() - 1]->Add(tl);
		Transform* tr = new Transform();
		tr->move(D3DXVECTOR3(0, 0, 10));
		com[com.size() - 1]->Add(tr);
		tr->ChangeParentBB();
		com[com.size() - 1]->SetParent(NULL);

	}
	/*
	in->setMapKey("Left", DIK_A);
	in->setMapKey("Left", DIK_LEFTARROW);
	in->setMapKey("Right", DIK_D);
	in->setMapKey("Right", DIK_RIGHTARROW);
	in->setMapKey("Space", DIK_SPACE);
	*/
}

void PlayState::update(LPDIRECT3DDEVICE9 dev, float deltaTime)
{
	if (Test == 6)
	{
		UINT passes;
		//com[0]->GetComponent<Transform>()->rotate(D3DXVECTOR3(0, 1, 0));
		effect->Begin(&passes, 0);
		effect->BeginPass(0);
		sls->Update(deltaTime);
		cam->Update(deltaTime);
		for (int i = 0; i < com.size(); i++)
		{
			com[i]->Update(deltaTime);
		}
		sls->Render(dev, effect);
		cam->Render(dev, effect);
		for (int i = 0; i < com.size(); i++)
		{
			com[i]->Render(dev, effect);
		}
		effect->EndPass();


		effect->BeginPass(1);
		sls2->Update(deltaTime);
		cam->Update(deltaTime);
		for (int i = 0; i < com.size(); i++)
		{
			com[i]->Update(deltaTime);
		}
		sls2->Render(dev, effect);
		cam->Render(dev, effect);
		for (int i = 0; i < com.size(); i++)
		{
			com[i]->Render(dev, effect);
		}
		effect->EndPass();
		effect->End();
	}
	else if (Test == 7)
	{
		com[0]->GetComponent<Transform>()->rotate(D3DXVECTOR3(1, 0, 0));
		com[0]->GetComponent<Transform>()->move(D3DXVECTOR3(0.01, 0, 0));
		com[0]->GetComponent<Transform>()->scale(D3DXVECTOR3(0.0001, 0, 0));
		cam->Update(deltaTime);
		for (int i = 0; i < com.size(); i++)
		{
			com[i]->Update(deltaTime);
		}
		cam->Render(dev, effect);
		for (int i = 0; i < com.size(); i++)
		{
			com[i]->Render(dev, effect);
		}

		timer += deltaTime;
		if (timer > 20 && !timertrigger1)
		{
			timertrigger1 = true;
			newcomp = new Composite();
			BoundingBox* bb = new BoundingBox();
			LoadModel* mod2 = new LoadModel("../Assets/bus.obj", dev, bb);
			Component* tl = new Renderer(mod2->getModel(), dev, cam);
			tl->bb->Combine(bb);
			tl->bb->Refresh();
			newcomp->Add(tl);
			Transform* tr = new Transform();
			tr->move(D3DXVECTOR3(10, 10, 10));
			newcomp->Add(tr);
			tr->ChangeParentBB();
			com[com.size() - 1]->Add(newcomp);

		}/*
		else if (timer > 10 && !timertrigger2)
		{
			timertrigger2 = true;
			com[com.size() - 1]->Remove(newcomp);

		}*/
	}
	else if (Test == 5)
	{
		UINT passes;
		com[0]->GetComponent<Transform>()->rotate(D3DXVECTOR3(0, 1, 0));
		effect->Begin(&passes, 0);
		effect->BeginPass(0);
		ls->Update(deltaTime);
		cam->Update(deltaTime);
		for (int i = 0; i < com.size(); i++)
		{
			com[i]->Update(deltaTime);
		}
		ls->Render(dev, effect);
		cam->Render(dev, effect);
		for (int i = 0; i < com.size(); i++)
		{
			com[i]->Render(dev, effect);
		}
		effect->EndPass();
		effect->End();
	}
	else if(Test == 4 || Test == 3)
	{
		UINT passes;
		effect->Begin(&passes, 0);
		effect->BeginPass(0);
		//com[0]->GetComponent<Transform>()->move(D3DXVECTOR3(0.01, 0, 0));
		effect->SetFloat("num", num);
		cam->Update(deltaTime);
		for (int i = 0; i < com.size(); i++)
		{
			com[i]->Update(deltaTime);
		}
		cam->Render(dev, effect);
		for (int i = 0; i < com.size(); i++)
		{
			com[i]->Render(dev, effect);
		}
		effect->EndPass();
		effect->End();
	}
	else
	{
		cam->Update(deltaTime);
		for (int i = 0; i < com.size(); i++)
		{
			com[i]->Update(deltaTime);
		}
		cam->Render(dev, effect);
		for (int i = 0; i < com.size(); i++)
		{
			com[i]->Render(dev, effect);
		}

		if (Test == 2)
		{
			com[0]->GetComponent<Transform>()->move(D3DXVECTOR3(0.01, 0, 0));
		}
	}
	/*
	obj[0]->draw(dev, Game::deltaTime);
	tm->update(dev);
	
	in->update();
	if (in->Pressed("Right"))
	{
		num++;
	}
	if (in->Pressed("Left"))
	{
		num--;
	}
	if (in->JustPressed("Space"))
	{
		up++;
	}
	dev->SetRenderState(D3DRS_ALPHABLENDENABLE, false);
	obj[0]->setMov(D3DXVECTOR3(num, up, 0.1f));
	obj[0]->setRot(D3DXVECTOR3(0, 0, 0));
	obj[0]->setSca(D3DXVECTOR3(0.05f, 0.05f, 0.05f));
	obj[0]->draw(dev);
	obj[1]->draw(dev);
	obj[2]->draw(dev);
	
	float rotZ = D3DXToRadian(num);
	D3DXMATRIX obj1;
	D3DXMATRIX obj2;
	D3DXMATRIX obj3;
	D3DXMATRIX obj4;
	D3DXVECTOR3 objdif;
	for (int i = 0; i < obj.size(); i++)
	{
		switch (i)
		{
		case 0:
			obj.back()->setMov(D3DXVECTOR3(0, 0, 5));
			obj.back()->setRot(D3DXVECTOR3(0, 0, rotZ));
			objdif = obj.back()->getMov();
			right = obj.back()->getRight();
			break;
		case 1:
			obj.back()->setMov(D3DXVECTOR3(0, 0, 5));
			break;
		case 2:
			obj.back()->setMov(D3DXVECTOR3(0.5f, 0.5f, 5));
			break;
		case 3:
			obj.back()->setMov(obj4Pos);

			//Calculo cuanto me falta para llegar al desinto (objPos, siendo mi posicion obj4Pos);
			D3DXVECTOR3 diff = objdif - obj4Pos;

			//Calculo el largo de lo que me falta (o sea, la distancia)
			//float dist = D3DXVec3Length(&diff);

			//Obtengo la direcci�n hacia mi destino normalizando el vector
			//D3DXVECTOR3 dir = diff / dist;

			//Esto mueve hacia el destino a una velocidad fija
			//obj4Pos += dir * 0.1;

			//Esto mueve hacia el destino con una velocidad directamente proporcional a la distancia
			float dist = sqrt(diff.x * diff.x + diff.y * diff.y + diff.z * diff.z);
			if (dist > 1) obj4Pos += diff * 0.05;
			obj4 = GetModelMatrix(
				obj4Pos,
				D3DXVECTOR3(0, 0, 0),
				D3DXVECTOR3(0.5f, 0.5f, 0.5f));
			obj.back()->setMat(obj4);
			break;
		}
		obj.back()->draw(dev);
		obj.push_front(obj.back());
		obj.pop_back();
	}*/
	
}
