#include "stdafx.h"
#include "PlayState.h"
#include <vld.h>

int APIENTRY _tWinMain(HINSTANCE hInstance,
                     HINSTANCE hPrevInstance,
                     LPTSTR    lpCmdLine,
                     int       nCmdShow)
{
	Game* p = new PlayState();
	p->run(hInstance, hPrevInstance, lpCmdLine, nCmdShow);
	delete p;
}
