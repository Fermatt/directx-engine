#ifndef PLAYSTATE_H
#define PLAYSTATE_H
#include "Game.h"
#include "Tilemap.h"
#include "Player.h"
#include "Camara.h"
#include "LightSetter.h"
#include "SpotLightSetter.h"

class PlayState :
	public Game
{
private:
	int Test = 0;
	float num = 0;
	float up = 0;
	Camara* cam;
	Model* mod;
	LightSetter* ls;
	SpotLightSetter* sls;
	SpotLightSetter* sls2;
	std::vector<Objeto*> obj;
	std::vector<Composite*> com;

	Composite* newcomp;

	Tilemap* tm;

	float timer = 0;

	bool timertrigger1 = false;
	bool timertrigger2 = false;
protected:
	void create();
	void update(LPDIRECT3DDEVICE9 dev, float deltaTime);
public:
	PlayState(void);
	~PlayState(void);
};
#endif
