#ifndef TEXTURE_H
#define TEXTURE_H
#include "Component.h"
#include "Vertex.h"

class Texture : public Component
{
private:
	LPDIRECT3DTEXTURE9 tex;
	D3DXVECTOR2 texSca;
	D3DXVECTOR2 texMov;
	D3DXVECTOR4 color;
	D3DXMATRIX matTex;
	int wrap;
	int filter;
	bool blendBool;
	int blendType;
public:
	Texture(LPCWSTR _str, LPDIRECT3DDEVICE9 dev);
	~Texture();
	void Render(LPDIRECT3DDEVICE9 dev, LPD3DXEFFECT effect) override;
	LPDIRECT3DTEXTURE9 getTex();
	void setTexSca(D3DXVECTOR2 _texSca);
	void setTexMov(D3DXVECTOR2 _texMov);
	void setColor(D3DXVECTOR4 _color);
	void setWrapToMirror();
	void setWrapToBorder();
	void setWrapToClamp();
	void setWrapToForceDWord();
	void setWrapToMirrorOnce();
	void setWrapToWrap();
	void setFilterToNone();
	void setFilterToPoint();
	void setFilterToLinear();
	void setFilterToAnisotropic();
	void setFilterToPyramidaLQuad();
	void setFilterToGaussianQuad();
	void setBlendBool(bool b);
	void setBlendTypeToAdditive();
	void setBlendTypeToMultiply();
	void setBlendTypeToAlpha();
};

#endif