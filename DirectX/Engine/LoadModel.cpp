#include "stdafx.h"
#include "LoadModel.h"

#include <vector>


LoadModel::LoadModel(std::string s, LPDIRECT3DDEVICE9 dev, BoundingBox* bb)
{
	vector<Vertex> vertexes;
	vector<WORD> indexes;

	vector<D3DXVECTOR3> positions;
	vector<D3DXVECTOR3> normals;
	vector<D3DXVECTOR2> uvs;

	FILE* file;
	fopen_s(&file, "../Assets/bus.obj", "r");

	//feof nos indica si estamos en el fin del archivo
	while (!feof(file))
	{
		//Obtengo la primer palabra despues de donde estoy parado
		//y la guardo en el array de chars
		char lineHeader[128];
		fscanf(file, "%s", lineHeader);

		//Si la primer palabra es V nos encontramos con una posicion
		//del vertice y la guardamos
		if (strcmp(lineHeader, "v") == 0)
		{
			D3DXVECTOR3 position;
			fscanf(file, "%f %f %f\n", &position.x, &position.y, &position.z);
			positions.push_back(position);

			bb->xMin = min(position.x, bb->xMin);
			bb->yMin = min(position.y, bb->yMin);
			bb->zMin = min(position.z, bb->zMin);

			bb->xMax = max(position.x, bb->xMax);
			bb->yMax = max(position.y, bb->yMax);
			bb->zMax = max(position.z, bb->zMax);
		}
		else if (strcmp(lineHeader, "vn") == 0)
		{
			D3DXVECTOR3 normal;
			fscanf(file, "%f %f %f\n", &normal.x, &normal.y, &normal.z);
			normals.push_back(normal);
		}
		else if (strcmp(lineHeader, "vt") == 0)
		{
			D3DXVECTOR2 uv;
			fscanf(file, "%f %f\n", &uv.x, &uv.y);
			uvs.push_back(uv);
		}
		else if (strcmp(lineHeader, "f") == 0)
		{
			Vertex vertex;
			int posIndex, uvIndex, normalIndex;

			fscanf(file, "%d/%d/%d ", &posIndex, &uvIndex, &normalIndex);
			vertex.x = positions[posIndex - 1].x;
			vertex.y = positions[posIndex - 1].y;
			vertex.z = positions[posIndex - 1].z;
			vertex.nx = normals[normalIndex - 1].x;
			vertex.ny = normals[normalIndex - 1].y;
			vertex.nz = normals[normalIndex - 1].z;
			vertex.tu = uvs[uvIndex - 1].x;
			vertex.tv = uvs[uvIndex - 1].y;
			int t = -1;
			for (int i = 0; i < vertexes.size(); i++)
			{
				if (vertexes[i].x == vertex.x &&
					vertexes[i].y == vertex.y &&
					vertexes[i].z == vertex.z &&
					vertexes[i].nx == vertex.nx &&
					vertexes[i].ny == vertex.ny &&
					vertexes[i].nz == vertex.nz &&
					vertexes[i].tu == vertex.tu &&
					vertexes[i].tv == vertex.tv)
				{
					t = i;
				}
			}
			if (t >= 0)
			{
				indexes.push_back(t);
			}
			else
			{
				indexes.push_back(vertexes.size());
				vertexes.push_back(vertex);
			}

			fscanf(file, "%d/%d/%d ", &posIndex, &uvIndex, &normalIndex);
			vertex.x = positions[posIndex - 1].x;
			vertex.y = positions[posIndex - 1].y;
			vertex.z = positions[posIndex - 1].z;
			vertex.nx = normals[normalIndex - 1].x;
			vertex.ny = normals[normalIndex - 1].y;
			vertex.nz = normals[normalIndex - 1].z;
			vertex.tu = uvs[uvIndex - 1].x;
			vertex.tv = uvs[uvIndex - 1].y;
			t = -1;
			for (int i = 0; i < vertexes.size(); i++)
			{
				if (vertexes[i].x == vertex.x &&
					vertexes[i].y == vertex.y &&
					vertexes[i].z == vertex.z &&
					vertexes[i].nx == vertex.nx &&
					vertexes[i].ny == vertex.ny &&
					vertexes[i].nz == vertex.nz &&
					vertexes[i].tu == vertex.tu &&
					vertexes[i].tv == vertex.tv)
				{
					t = i;
				}
			}
			if (t >= 0)
			{
				indexes.push_back(t);
			}
			else
			{
				indexes.push_back(vertexes.size());
				vertexes.push_back(vertex);
			}

			fscanf(file, "%d/%d/%d\n", &posIndex, &uvIndex, &normalIndex);
			vertex.x = positions[posIndex - 1].x;
			vertex.y = positions[posIndex - 1].y;
			vertex.z = positions[posIndex - 1].z;
			vertex.nx = normals[normalIndex - 1].x;
			vertex.ny = normals[normalIndex - 1].y;
			vertex.nz = normals[normalIndex - 1].z;
			vertex.tu = uvs[uvIndex - 1].x;
			vertex.tv = uvs[uvIndex - 1].y;
			t = -1;
			for (int i = 0; i < vertexes.size(); i++)
			{
				if (vertexes[i].x == vertex.x &&
					vertexes[i].y == vertex.y &&
					vertexes[i].z == vertex.z &&
					vertexes[i].nx == vertex.nx &&
					vertexes[i].ny == vertex.ny &&
					vertexes[i].nz == vertex.nz &&
					vertexes[i].tu == vertex.tu &&
					vertexes[i].tv == vertex.tv)
				{
					t = i;
				}
			}
			if (t >= 0)
			{
				indexes.push_back(t);
			}
			else
			{
				indexes.push_back(vertexes.size());
				vertexes.push_back(vertex);
			}
		}
	}
	bb->Refresh();

	model = new VbIb();

	model->lengthVb = vertexes.size();
	model->lengthIb = indexes.size();
	model->bb = bb;
	model->exists = true;

	dev->CreateVertexBuffer(vertexes.size() * sizeof(Vertex), 0, MYFVF, D3DPOOL_MANAGED, &model->vb, NULL);
	dev->CreateIndexBuffer(indexes.size() * sizeof(WORD), 0, D3DFMT_INDEX16, D3DPOOL_MANAGED, &model->ib, NULL);

	VOID* lockedData = NULL;
	model->vb->Lock(0, 0, (void**)&lockedData, 0);
	memcpy(lockedData, vertexes.data(), vertexes.size() * sizeof(Vertex));
	model->vb->Unlock();

	model->ib->Lock(0, 0, (void**)&lockedData, 0);
	memcpy(lockedData, indexes.data(), indexes.size() * sizeof(WORD));
	model->ib->Unlock();
}


LoadModel::~LoadModel()
{
}

VbIb* LoadModel::getModel()
{
	return model;
}
