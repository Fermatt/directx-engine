#include <iostream>
#include "stdafx.h"
#include "Renderer.h"
#include "Composite.h"
#include "Transform.h"
#include "BoundingBox.h"

Renderer::Renderer(VbIb * _model, LPDIRECT3DDEVICE9 dev, Camara* _c)
{
	model = _model;/*
	D3DXCreateEffectFromFile(
		dev, _effect, NULL, NULL,
		D3DXSHADER_ENABLE_BACKWARDS_COMPATIBILITY,
		NULL, &effect, NULL);*/
	c = _c;
	D3DXMatrixIdentity(&matTrans);
}

Renderer::~Renderer()
{
}

void Renderer::UpdateComposite(float deltaTime)
{

}

void Renderer::RenderComposite(LPDIRECT3DDEVICE9 dev, LPD3DXEFFECT effect)
{
	if (GetParent())
	{
		Transform* t = GetParent()->GetComponent<Transform>();
		D3DXMATRIX matTrans2 = t->GetModelMatrix();
		if (matTrans2 != matTrans)
		{
			matTrans = matTrans2;
			GetParent()->bb = model->bb;
			GetParent()->bb = GetParent()->bb->Transform(matTrans);
			GetParent()->bb->Refresh();
			GetParent()->MergeChildBox();
		}
		if (GetParent()->GetParent() != nullptr)
		{
			Renderer* r = GetParent()->GetParent()->GetComponent<Renderer>();
			//BoundingBox* bbParent = GetParent()->GetParent()->GetComponent<BoundingBox>();
			if (r && r != this)
			{
				matTrans = (r->getMatTrans() * matTrans);
			}
		}
	}

	D3DXMATRIX mvp = matTrans * c->getViewMatrix() * c->getProjMatrix();

	if (effect)
	{
		effect->SetMatrix("mvp", &mvp);
	}
	else
		dev->SetTransform(D3DTS_WORLD, &matTrans);
	dev->SetFVF(MYFVF);
	dev->SetIndices(model->ib);
	dev->SetStreamSource(0, model->vb, 0, sizeof(Vertex));
	
	dev->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, model->lengthIb, 0, model->lengthIb / 3);
}

D3DXMATRIX Renderer::getMatTrans()
{
	return matTrans;
}

void Renderer::setMatTrans(D3DXMATRIX d)
{
	matTrans = d;
}
