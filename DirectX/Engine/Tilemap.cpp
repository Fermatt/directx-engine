#include "stdafx.h"
#include "Tilemap.h"
#include "Game.h"
//#include "Vertex.h"

Tilemap::Tilemap(std::vector<std::vector<int>> _tilemap, std::vector<Texture*> _tex, int _ancho, int _alto, VbIb* _model) : tex(_tex), ancho(_ancho), alto(_alto), model(_model)
{
	tilemap[0] = _tilemap;
}

Tilemap::Tilemap(std::vector < std::vector<std::vector<int>>> _tilemap, std::vector<Texture*> _tex, int _ancho, int _alto, VbIb* _model) : tilemap(_tilemap), tex(_tex), ancho(_ancho), alto(_alto), model(_model)
{
}

Tilemap::~Tilemap()
{
}

void Tilemap::update(LPDIRECT3DDEVICE9 dev)
{
	D3DXMATRIX matSca;
	D3DXMatrixScaling(&matSca, ancho, alto, 1);
	dev->SetFVF(MYFVF);
	dev->SetIndices(model->ib);
	dev->SetStreamSource(0, model->vb, 0, sizeof(Vertex));

	for (int capa = 0; capa < tilemap.size(); capa++)
	{
		for (int indiceFila = 0; indiceFila < tilemap[capa].size(); indiceFila++)
		{
			std::vector<int>* fila = &tilemap[capa][indiceFila];

			for (int indiceColumna = 0; indiceColumna < fila->size(); indiceColumna++)
			{
				int celda = fila->at(indiceColumna);
				if (celda >= 0)
				{
					LPDIRECT3DTEXTURE9 loadTex = tex[celda]->getTex();
					dev->SetTexture(0, loadTex);

					float posX = ancho * indiceColumna;
					float posY = alto * indiceFila;
					D3DXMATRIX matTrans;
					D3DXMatrixTranslation(&matTrans, posX, -posY, 3);

					D3DXMATRIX mat = matSca * matTrans;
					dev->SetTransform(D3DTS_WORLD, &mat);

					dev->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, 4, 0, 2);
				}
			}
		}
	}
}
