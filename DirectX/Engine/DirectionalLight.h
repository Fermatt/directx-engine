#pragma once
#include "Composite.h"
class DirectionalLight :
	public Composite
{
private:
	LPD3DXEFFECT effect;
	D3DXVECTOR4 lightDir;
	D3DXVECTOR4 lightCol;
	D3DXVECTOR4 ambientCol;
public:
	DirectionalLight(LPD3DXEFFECT _effect);
	~DirectionalLight();
	void UpdateComposite(float deltaTime) override;
	void RenderComposite(LPDIRECT3DDEVICE9 dev, LPD3DXEFFECT effect) override;
	void setEffect(LPD3DXEFFECT _effect);
	LPD3DXEFFECT getEffect();
	void openEffect();
	void closeEffect();
};

