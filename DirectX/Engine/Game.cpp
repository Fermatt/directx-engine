#include "StdAfx.h"
#include "Game.h"

Game::Game()
{

}


Game::~Game()
{
}

int Game::getMs()
{
	time_point<system_clock> actualTime = system_clock::now();
	system_clock::duration duration = actualTime.time_since_epoch();
	milliseconds ms = duration_cast<milliseconds>(duration);
	return ms.count();
}

LRESULT CALLBACK Game::WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	if (message == WM_DESTROY)
	{
		PostQuitMessage(0);
		return 0;
	}
	return DefWindowProc(hWnd, message, wParam, lParam);
}

void APIENTRY Game::run(_In_     HINSTANCE hInstance,
					   _In_opt_ HINSTANCE hPrevInstance,
					   _In_     LPTSTR    lpCmdLine,
					   _In_     int       nCmdShow)
{
	WNDCLASSEX wcex;

	ZeroMemory(&wcex, sizeof(WNDCLASSEX));

	
	wcex.cbSize = sizeof(WNDCLASSEX); //Tama�o en bytes
	wcex.style = CS_HREDRAW | CS_VREDRAW; //Estilo de la ventana
	wcex.lpfnWndProc = WndProc; //Funcion de manejo de mensajes de ventana
	wcex.hInstance = hInstance; //Numero de instancia
	wcex.hCursor = LoadCursor(nullptr, IDC_ARROW); //Cursor del mouse
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1); //Color de fondo
	wcex.lpszClassName = L"GameWindowClass"; //Nombre del tipo (clase) de ventana

	RegisterClassEx(&wcex);
	
	HWND hWnd = CreateWindowEx(	0, //Flags extra de estilo
								L"GameWindowClass", //Nombre del tipo de ventana a crear
								L"Title", //Titulo de la barra
								WS_OVERLAPPEDWINDOW, //Flags de estilos
								0, //X
								0, //Y
								640, //Ancho
								480, //Alto
								NULL, //Ventana padre
								NULL, //Menu
								hInstance, //Numero de proceso
								NULL); //Flags de multi ventana

	ShowWindow(hWnd, nCmdShow); //Muestro la ventana
	UpdateWindow(hWnd); //La actualizo para que se vea

	LPDIRECT3D9 d3d = Direct3DCreate9(D3D_SDK_VERSION);

	D3DPRESENT_PARAMETERS d3dpp;
	ZeroMemory(&d3dpp, sizeof(D3DPRESENT_PARAMETERS));
	d3dpp.Windowed = true;
	d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;
	d3dpp.hDeviceWindow = hWnd;

	d3d->CreateDevice(D3DADAPTER_DEFAULT, //Cual placa de vido
					  D3DDEVTYPE_HAL, //Soft o hard
					  hWnd, //Ventana
					  D3DCREATE_HARDWARE_VERTEXPROCESSING, //Proceso de vertices por soft o hard
					  &d3dpp, //Los parametros de buffers
					  &dev); //El device que se crea
	
	in = new InputKey(hInstance, hWnd, dev);

	create();

	//dev->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE);

	int lastFrameMs = getMs();


	while (true)
	{

		float actualMs = getMs();
		deltaTime = (actualMs - lastFrameMs) / 1000.0f;

		lastFrameMs = actualMs;

		MSG msg;
		while (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&msg); 
			DispatchMessage(&msg);
		}

		if (msg.message == WM_QUIT)
		{
			break;
		}
		dev->Clear(0, NULL, D3DCLEAR_TARGET, D3DCOLOR_ARGB(50, 50, 0, 50), 1.0f, 0);
		dev->BeginScene();

		//Draw
		update(dev, deltaTime);

		dev->EndScene();
		dev->Present(NULL, NULL, NULL, NULL);
	}
	dev->Release();
	d3d->Release();
	delete in;
}