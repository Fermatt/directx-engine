#ifndef FRAME_H
#define FRAME_H

class Frame
{
private:
	int x;
	int y;
	int width;
	int height;
public:
	Frame(int _x, int _y, int _width, int _height);
	~Frame();
	int getX();
	int getY();
	int getWidth();
	int getHeight();
};

#endif

