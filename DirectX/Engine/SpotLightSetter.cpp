#include "stdafx.h"
#include "SpotLightSetter.h"


SpotLightSetter::SpotLightSetter(int coseno)
{
	Add(new Transform());
	cosen = coseno;
	num = cosen / 180;
}


SpotLightSetter::~SpotLightSetter()
{
}

void SpotLightSetter::UpdateComposite(float deltaTime)
{
	num += 0.1f;
	lightRange = 5;
	lightPos = D3DXVECTOR4(cos(num + cosen) * 0.5f, 0, num* 0.1f, 0);
	lightDir = D3DXVECTOR4(0, 0, 1, 0);
	lightCol = D3DXVECTOR4(1, 1, 1, 0);
	ambientCol = D3DXVECTOR4(0.3f, 0, 0, 0);
}

void SpotLightSetter::RenderComposite(LPDIRECT3DDEVICE9 dev, LPD3DXEFFECT effect)
{
	effect->SetInt("lightType", 2);
	effect->SetFloat("lightRange", lightRange);
	effect->SetVector("lightPos", &lightPos);
	effect->SetVector("lightDir", &lightDir);
	effect->SetVector("lightCol", &lightCol);
	effect->SetVector("ambientCol", &ambientCol);
}

void SpotLightSetter::setLightDir(D3DXVECTOR4 _lightDir)
{
	lightDir = _lightDir;
}

void SpotLightSetter::setLightCol(D3DXVECTOR4 _lightCol)
{
	lightCol = _lightCol;
}

void SpotLightSetter::setAmbientCol(D3DXVECTOR4 _ambientCol)
{
	ambientCol = _ambientCol;
}