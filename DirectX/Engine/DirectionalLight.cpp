#include "stdafx.h"
#include "DirectionalLight.h"


DirectionalLight::DirectionalLight(LPD3DXEFFECT _effect)
{
	effect = _effect;
	lightDir = D3DXVECTOR4(0, 0, 1, 0);
	lightCol = D3DXVECTOR4(1, 1, 1, 0);
	ambientCol = D3DXVECTOR4(0.3f, 0, 0, 0);
}


DirectionalLight::~DirectionalLight()
{
}

void DirectionalLight::UpdateComposite(float deltaTime)
{
	effect->SetVector("lightDir", &lightDir);
	effect->SetVector("lightCol", &lightCol);
	effect->SetVector("ambientCol", &ambientCol);
}

void DirectionalLight::RenderComposite(LPDIRECT3DDEVICE9 dev, LPD3DXEFFECT effect)
{

}

void DirectionalLight::setEffect(LPD3DXEFFECT _effect)
{
	effect = _effect;
}

LPD3DXEFFECT DirectionalLight::getEffect()
{
	return effect;
}

void DirectionalLight::openEffect()
{
	UINT passes;
	effect->Begin(&passes, 0);
	effect->BeginPass(0);
}

void DirectionalLight::closeEffect()
{
	effect->EndPass();
	effect->End();
}

