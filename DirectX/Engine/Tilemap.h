#ifndef TILEMAP_H
#define TILEMAP_H
#include <vector>
#include "Texture.h"
#include "Model.h"

class Tilemap
{
private:
	std::vector < std::vector<std::vector<int>>> tilemap;
	std::vector<Texture*> tex;
	VbIb* model;
	int ancho;
	int alto;
public:
	Tilemap(std::vector<std::vector<int>> _tilemap, std::vector<Texture*> _tex, int _ancho, int _alto, VbIb* _model);
	Tilemap(std::vector < std::vector<std::vector<int>>> _tilemap, std::vector<Texture*> _tex, int _ancho, int _alto, VbIb* _model);
	~Tilemap();
	void update(LPDIRECT3DDEVICE9 dev);
};

#endif