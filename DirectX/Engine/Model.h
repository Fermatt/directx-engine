#ifndef MODEL_H
#define MODEL_H
#include "Vertex.h"
#include <iostream>

using namespace std;

class Model
{
private:
	VbIb* Circle;
	VbIb* Square;
	VbIb* Triangle;

	LPDIRECT3DDEVICE9 dev;
public:
	Model(LPDIRECT3DDEVICE9 _dev);
	~Model();
	VbIb* getCircle(float radius);
	VbIb* getSquare();
	VbIb* getTriangle();
	void sendMemory(VbIb* form, Vertex v[], WORD w[]);
};

#endif