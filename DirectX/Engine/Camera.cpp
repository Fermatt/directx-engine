#include "StdAfx.h"
#include "Camera.h"


Camera::Camera()
{
	angle = 60;
	width = 640;
	height = 480;
	minD = 0.0f;
	maxD = 100;
}

Camera::Camera(int _angle, float _width, float _height, float _minD, float _maxD)
{
	angle = _angle;
	width = _width;
	height = _height;
	minD = _minD;
	maxD = _maxD;
}


Camera::~Camera()
{
}

void Camera::Update(LPDIRECT3DDEVICE9 dev)
{
	view = GetViewMatrix(
			D3DXVECTOR3(0, 0, 0),
			D3DXVECTOR3(0, 0, 0));
	D3DXMatrixPerspectiveFovLH(
		&proj,
		D3DXToRadian(angle), //El angulo de apertura de la camara
		(float)width / height, //El ancho de la pantalla dividido el alto (relaci�n de aspect)
		minD, //La distancia minima del objeto para verse
		maxD); //La distancia maxima del objeto para verse

	dev->SetTransform(D3DTS_VIEW, &view);
	dev->SetTransform(D3DTS_PROJECTION, &proj);
}


D3DXMATRIX Camera::GetViewMatrix(D3DXVECTOR3 pos, D3DXVECTOR3 rot)
{
	D3DXMATRIX transMat;
	D3DXMatrixIdentity(&transMat);
	transMat._41 = -pos.x;
	transMat._42 = -pos.y;
	transMat._43 = -pos.z;

	D3DXMATRIX rotZMat;
	D3DXMatrixIdentity(&rotZMat);
	rotZMat._11 = cos(-rot.z);
	rotZMat._12 = sin(-rot.z);
	rotZMat._21 = -sin(-rot.z);
	rotZMat._22 = cos(-rot.z);

	return transMat * rotZMat;
}