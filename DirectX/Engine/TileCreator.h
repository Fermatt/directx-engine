#ifndef TILE_CREATOR_H
#define TILE_CREATOR_H
#include "Component.h"
#include "Vertex.h"
#include "Texture.h"
#include <vector>

class TileCreator :
	public Component
{
private:
	std::vector < std::vector<std::vector<int>>> tilemap;
	std::vector<Texture*> tex;
	VbIb* model;
	int ancho;
	int alto;
	D3DXVECTOR2 worldPos;
	D3DXVECTOR2 worldSca;
public:
	TileCreator(std::vector<std::vector<int>> _tilemap, std::vector<Texture*> _tex, int _ancho, int _alto, VbIb* _model);
	TileCreator(std::vector < std::vector<std::vector<int>>> _tilemap2, std::vector<Texture*> _tex, int _ancho, int _alto, VbIb* _model);
	~TileCreator();
	void Update(float deltaTime) override;
	void Render(LPDIRECT3DDEVICE9 dev, LPD3DXEFFECT effect) override;
};

#endif