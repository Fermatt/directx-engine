#ifndef GAME_H
#define GAME_H
#include "EngineApi.h"
#include "Camera.h"
#include "Camara.h"
#include "Vertex.h"
#include "Model.h"
#include "InputKey.h"
#include "Objeto.h"
#include "ObjetoAnim.h"
#include "Texture.h"
#include <chrono>
#include <map>
#include <vector>
#include <time.h>
#include <random>
#include <list>

#include <d3d9.h> //Busca el header de directx en los path
#pragma comment (lib, "d3d9.lib") //Incluyo la lib a mi proyecto

using namespace std::chrono;

class ENGINE_API Game
{
protected:
	LPDIRECT3DDEVICE9 dev;
	InputKey *in;
	virtual void create() = 0;
	virtual void update(LPDIRECT3DDEVICE9 dev, float deltaTime) = 0;
	int getMs();
	float deltaTime;
	LPD3DXEFFECT effect;
public:
	Game();
	~Game();
	void APIENTRY run(_In_     HINSTANCE hInstance,
					   _In_opt_ HINSTANCE hPrevInstance,
					   _In_     LPTSTR    lpCmdLine,
					   _In_     int       nCmdShow);
	static LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);

	//static Camara* MainCam;
	
};

#endif