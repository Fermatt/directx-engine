#ifndef INPUTKEY_H
#define INPUTKEY_H

#include <map>
#include <vector>
#include <iostream>
#include <d3d9.h> //Busca el header de directx en los path
#pragma comment (lib, "d3d9.lib") //Incluyo la lib a mi proyecto
#include <dinput.h> //Busca el header de directx en los path
#pragma comment (lib, "dinput8.lib") //Incluyo la lib a mi proyecto
#pragma comment (lib, "dxguid.lib") //Incluyo la lib a mi proyecto

class InputKey
{
private:
	LPDIRECTINPUT8 dip;
	LPDIRECTINPUTDEVICE8 keyDev;
	byte keys[256];
	byte prevKeys[256];
	std::map<std::string, std::vector<int>> m;
public:
	InputKey(HINSTANCE hInstance, HWND hWnd, LPDIRECT3DDEVICE9 dev);
	~InputKey();
	void update();
	bool Pressed(int k);
	bool JustPressed(int k);
	bool JustReleased(int k);
	bool Released(int k);
	bool Pressed(std::string s);
	bool JustPressed(std::string s);
	bool JustReleased(std::string s);
	bool Released(std::string s);
	void setMapKey(std::string s, int k);
};

#endif