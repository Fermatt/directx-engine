#include "stdafx.h"
#include "Texture.h"


Texture::Texture(LPCWSTR _str, LPDIRECT3DDEVICE9 dev)
{
	D3DXCreateTextureFromFile(dev, _str, &tex);
	texSca = D3DXVECTOR2(1, 1);
	texMov = D3DXVECTOR2(0, 0);
	wrap = 0;
	filter = 0;
	blendBool = false;
	blendType = 0;
	color = D3DXVECTOR4(1, 0, 0, 0);
}


Texture::~Texture()
{
}

void Texture::Render(LPDIRECT3DDEVICE9 dev, LPD3DXEFFECT effect)
{
	D3DXMatrixIdentity(&matTex);
	matTex._11 = texSca.x;
	matTex._22 = texSca.y;
	matTex._31 = texMov.x;
	matTex._32 = texMov.y;
	dev->SetRenderState(D3DRS_ALPHABLENDENABLE, blendBool);
	if (blendBool)
	{
		switch (blendType)
		{
		case 0:
			dev->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_ONE);
			dev->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_ONE);
			break;
		case 1:
			dev->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_ZERO);
			dev->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_SRCCOLOR);
			break;
		case 2:
			dev->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
			dev->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
			break;
		}
	}
	dev->SetTexture(0, tex);
	dev->SetSamplerState(0, D3DSAMP_ADDRESSU, wrap);
	dev->SetSamplerState(0, D3DSAMP_ADDRESSV, wrap);
	dev->SetSamplerState(0, D3DSAMP_MINFILTER, filter);
	dev->SetSamplerState(0, D3DSAMP_MAGFILTER, filter);
	dev->SetSamplerState(0, D3DSAMP_MIPFILTER, filter);
	dev->SetTextureStageState(0, D3DTSS_TEXTURETRANSFORMFLAGS, D3DTTFF_COUNT2);
	dev->SetTransform(D3DTS_TEXTURE0, &matTex);
	if(effect)
		effect->SetVector("_Color", &color);
}

LPDIRECT3DTEXTURE9 Texture::getTex()
{
	return tex;
}

void Texture::setTexSca(D3DXVECTOR2 _texSca)
{
	texSca = _texSca;
}

void Texture::setTexMov(D3DXVECTOR2 _texMov)
{
	texMov = _texMov;
}

void Texture::setColor(D3DXVECTOR4 _color)
{
	color = _color;
}

void Texture::setWrapToMirror()
{
	wrap = D3DTEXTUREADDRESS::D3DTADDRESS_MIRROR;
}

void Texture::setWrapToBorder()
{
	wrap = D3DTEXTUREADDRESS::D3DTADDRESS_BORDER;
}

void Texture::setWrapToClamp()
{
	wrap = D3DTEXTUREADDRESS::D3DTADDRESS_CLAMP;
}

void Texture::setWrapToForceDWord()
{
	wrap = D3DTEXTUREADDRESS::D3DTADDRESS_FORCE_DWORD;
}

void Texture::setWrapToMirrorOnce()
{
	wrap = D3DTEXTUREADDRESS::D3DTADDRESS_MIRRORONCE;
}

void Texture::setWrapToWrap()
{
	wrap = D3DTEXTUREADDRESS::D3DTADDRESS_WRAP;
}

void Texture::setFilterToNone()
{
	filter = _D3DTEXTUREFILTERTYPE::D3DTEXF_NONE;
}

void Texture::setFilterToPoint()
{
	filter = _D3DTEXTUREFILTERTYPE::D3DTEXF_POINT;
}

void Texture::setFilterToLinear()
{
	filter = _D3DTEXTUREFILTERTYPE::D3DTEXF_POINT;
}

void Texture::setFilterToAnisotropic()
{
	filter = _D3DTEXTUREFILTERTYPE::D3DTEXF_ANISOTROPIC;
}

void Texture::setFilterToPyramidaLQuad()
{
	filter = _D3DTEXTUREFILTERTYPE::D3DTEXF_PYRAMIDALQUAD;
}

void Texture::setFilterToGaussianQuad()
{
	filter = _D3DTEXTUREFILTERTYPE::D3DTEXF_GAUSSIANQUAD;
}

void Texture::setBlendBool(bool b)
{
	blendBool = b;
}

void Texture::setBlendTypeToAdditive()
{
	blendType = 0;
}

void Texture::setBlendTypeToMultiply()
{
	blendType = 1;
}

void Texture::setBlendTypeToAlpha()
{
	blendType = 2;
}