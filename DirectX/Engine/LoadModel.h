#pragma once
#include "Vertex.h"
#include "BoundingBox.h"
#include <iostream>

using namespace std;

class LoadModel
{
private:
	VbIb* model;
public:
	LoadModel(string s, LPDIRECT3DDEVICE9 dev, BoundingBox* bb);
	~LoadModel();
	VbIb* getModel();
};

