struct VS_INPUT
{
	float4 pos : POSITION;
	float2 uv : TEXCOORD;
};

struct VS_OUTPUT
{
	float4 pos : SV_POSITION;
	float2 uv : TEXCOORD;
	float4 copyPos : TEXCOORD2;

};

struct PS_OUTPUT
{
	float4 color: COLOR;
};

VS_OUTPUT VS(VS_INPUT vertex)
{
	VS_OUTPUT fragment;
	fragment.pos = vertex.pos;

	//Move 0.5f
	
	fragment.pos.x -= 0.5f;
	fragment.pos.y -= 0.5f;

	//Shrink
	fragment.pos.x *= 0.5f;
	fragment.pos.y *= 0.5f;

	fragment.uv = vertex.uv;
	fragment.copyPos = fragment.pos;
	return fragment;
}

PS_OUTPUT PS(VS_OUTPUT fragment)
{
	PS_OUTPUT pixel;

	//ROJO
	pixel.color = float4(1, 0, 0, 0);

	//XYZ como RGB
	pixel.color = float4(fragment.copyPos.x, fragment.copyPos.y, fragment.copyPos.z, 0);


	return pixel;
}

technique RenderScene
{
	pass P0
	{
		VertexShader = compile vs_2_0 VS();
		PixelShader = compile ps_2_0 PS();
	}
}