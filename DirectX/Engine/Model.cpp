#include "StdAfx.h"
#include "Model.h"


Model::Model(LPDIRECT3DDEVICE9 _dev)
{
	dev = _dev;
	Circle = new VbIb();
	Square = new VbIb();
	Triangle = new VbIb();
	Circle->exists = false;
	Square->exists = false;
	Triangle->exists = false;
}

Model::~Model()
{
	if (Circle->exists)
	{
		Circle->vb->Release();
		Circle->ib->Release();
	}
	delete Circle;
	if (Square->exists)
	{
		Square->vb->Release();
		Square->ib->Release();
	}
	delete Square;
	if (Triangle->exists)
	{
		Triangle->vb->Release();
		Triangle->ib->Release();
	}
	delete Triangle;
}

VbIb* Model::getCircle(float radius)
{
	if(!Circle->exists)
	{
		Vertex vertexes[] =
		{
			{ radius, radius, 0.0f, D3DCOLOR_XRGB(255, 0, 0) },
			{ radius / 3, radius / 3, 0.0f, D3DCOLOR_XRGB(255, 0, 0) },
			{ radius * 0, radius, 0.0f, D3DCOLOR_XRGB(255, 0, 0) },
			{ radius / 3, radius * 2 - radius / 3, 0.0f, D3DCOLOR_XRGB(255, 0, 0) },
			{ radius, radius*2, 0.0f, D3DCOLOR_XRGB(255, 0, 0) },
			{ radius * 2 - radius / 3, radius * 2 - radius / 3, 0.0f, D3DCOLOR_XRGB(255, 0, 0) },
			{ radius * 2, radius, 0.0f, D3DCOLOR_XRGB(255, 0, 0) },
			{ radius * 2 - radius / 3, radius / 3, 0.0f, D3DCOLOR_XRGB(255, 0, 0) },
			{ radius, radius * 0, 0.0f, D3DCOLOR_XRGB(255, 0, 0) },
		};
		Circle->lengthVb = 9;
		WORD indexes[] = {  0, 1, 8, 
							1, 2, 8,
							2, 3, 8,
							3, 4, 8,
							4, 5, 8,
							5, 6, 8,
							6, 7, 8,
							7, 0, 8};
		Circle->lengthIb = 8*3;
		sendMemory(Circle, vertexes, indexes);
		Circle->exists = true;
	}
	return Circle;
}

VbIb* Model::getSquare()
{
	if(!Square->exists)
	{
		Vertex vertexes[] =
		{
			{ -0.5f, 0.5f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f},
			{ 0.5f, 0.5f, 0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f },
			{ 0.5f, -0.5f, 0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f },
			{ -0.5f, -0.5f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f },
		};
		WORD indexes[] = {  0, 1, 2, 
							2, 3, 0};
		Square->lengthVb = 4;
		Square->lengthIb = 6;
		sendMemory(Square, vertexes, indexes);
		Square->exists = true;
	}
	return Square;
}

VbIb* Model::getTriangle()
{
	if(!Triangle->exists)
	{
		Vertex vertexes[] =
		{
			{ 0.0f, 50.0f, 0.0f, D3DCOLOR_XRGB(0, 255, 0) },
			{ 50.0f, 50.0f, 0.0f, D3DCOLOR_XRGB(0, 255, 0) },
			{ 25.0f, 0.0f, 0.0f, D3DCOLOR_XRGB(0, 255, 0) },

		};
		WORD indexes[] = {  0, 1, 2};
		Triangle->lengthVb = 3;
		Triangle->lengthIb = 3;
		sendMemory(Triangle, vertexes, indexes);
		Triangle->exists = true;
	}
	return Triangle;
}

void Model::sendMemory(VbIb* form, Vertex v[], WORD w[])
{
	dev->CreateVertexBuffer(form->lengthVb * sizeof(Vertex),
		D3DUSAGE_WRITEONLY,
		MYFVF,
		D3DPOOL_MANAGED,
		&form->vb,
		NULL);
	dev->CreateIndexBuffer(
		form->lengthIb * sizeof(WORD),
		D3DUSAGE_WRITEONLY,
		D3DFMT_INDEX16,
		D3DPOOL_MANAGED,
		&form->ib,
		NULL);
	VOID *data;
	form->vb->Lock(0, 0, &data, 0);
	memcpy(data, v, form->lengthVb * sizeof(Vertex));
	form->vb->Unlock();
	form->ib->Lock(0, 0, &data, 0);
	memcpy(data, w, form->lengthIb * sizeof(WORD));
	form->ib->Unlock();
}

