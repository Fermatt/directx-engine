#include "stdafx.h"
#include "LightSetter.h"


LightSetter::LightSetter()
{
	Add(new Transform());
}

void LightSetter::UpdateComposite(float deltaTime)
{
	lightDir = D3DXVECTOR4 (0, 0, 1, 0);
	lightCol = D3DXVECTOR4 (1, 1, 1, 0);
	ambientCol = D3DXVECTOR4 (0.3f, 0, 0, 0);
}

void LightSetter::RenderComposite(LPDIRECT3DDEVICE9 dev, LPD3DXEFFECT effect)
{
	if (effect)
	{
		effect->SetInt("lightType", 0);
		effect->SetVector("lightDir", &lightDir);
		effect->SetVector("lightCol", &lightCol);
		effect->SetVector("ambientCol", &ambientCol);
	}
}

void LightSetter::setLightDir(D3DXVECTOR4 _lightDir)
{
	lightDir = _lightDir;
}

void LightSetter::setLightCol(D3DXVECTOR4 _lightCol)
{
	lightCol = _lightCol;
}

void LightSetter::setAmbientCol(D3DXVECTOR4 _ambientCol)
{
	ambientCol = _ambientCol;
}

LightSetter::~LightSetter()
{
}
