#include "stdafx.h"
#include "Component.h"


Component::Component()
{
	SetParent(NULL);
	bb = new BoundingBox();
}


Component::~Component()
{
}

void Component::Update(float deltaTime)
{
}

void Component::Render(LPDIRECT3DDEVICE9 dev, LPD3DXEFFECT effect)
{
}

void Component::SetParent(Composite * parent)
{
	this->parent = parent;
}

Composite * Component::GetParent()
{
	return parent;
}

