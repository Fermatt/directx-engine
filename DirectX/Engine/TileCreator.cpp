#include "stdafx.h"
#include "TileCreator.h"
#include "Transform.h"


TileCreator::TileCreator(std::vector<std::vector<int>> _tilemap, std::vector<Texture*> _tex, int _ancho, int _alto, VbIb* _model) : tex(_tex), ancho(_ancho), alto(_alto), model(_model)
{
	tilemap[0] = _tilemap;
}

TileCreator::TileCreator(std::vector < std::vector<std::vector<int>>> _tilemap2, std::vector<Texture*> _tex, int _ancho, int _alto, VbIb* _model) : tilemap(_tilemap2), tex(_tex), ancho(_ancho), alto(_alto), model(_model)
{
}


TileCreator::~TileCreator()
{
}

void TileCreator::Update(float deltaTime)
{
}

void TileCreator::Render(LPDIRECT3DDEVICE9 dev, LPD3DXEFFECT effect)
{
	if (GetParent())
	{
		Transform* t = GetParent()->GetComponent<Transform>();
		if (t)
		{
			worldPos.x = t->getPos().x;
			worldPos.y = t->getPos().y;
			worldSca.x = t->getSca().x;
			worldSca.y = t->getSca().y;
		}
		else
		{
			worldPos.x = 0;
			worldPos.y = 0;
			worldSca.x = 0;
			worldSca.y = 0;
		}
	}
	else
	{

	}
	D3DXMATRIX matSca;
	D3DXMatrixScaling(&matSca, ancho*worldSca.x, alto*worldSca.y, 1);
	dev->SetFVF(MYFVF);
	dev->SetIndices(model->ib);
	dev->SetStreamSource(0, model->vb, 0, sizeof(Vertex));

	for (int capa = 0; capa < tilemap.size(); capa++)
	{
		for (int indiceFila = 0; indiceFila < tilemap[capa].size(); indiceFila++)
		{
			std::vector<int>* fila = &tilemap[capa][indiceFila];

			for (int indiceColumna = 0; indiceColumna < fila->size(); indiceColumna++)
			{
				int celda = fila->at(indiceColumna);
				if (celda >= 0)
				{
					LPDIRECT3DTEXTURE9 loadTex = tex[celda]->getTex();
					dev->SetTexture(0, loadTex);

					float posX = ancho*worldSca.x * indiceColumna + worldPos.x;
					float posY = alto*worldSca.y * indiceFila + +worldPos.y;
					D3DXMATRIX matTrans;
					D3DXMatrixTranslation(&matTrans, posX, -posY, 3);

					D3DXMATRIX mat = matSca * matTrans;
					dev->SetTransform(D3DTS_WORLD, &mat);

					dev->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, 4, 0, 2);
				}
			}
		}
	}
}
