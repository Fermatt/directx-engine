#pragma once
#include "Composite.h"

struct Plane
{
	float a, b, c, d;
};

class Camara :
	public Composite
{
private:
	int angle;
	float width;
	float height;
	float minD;
	float maxD;
	D3DXMATRIX view;
	D3DXMATRIX proj;
	D3DXMATRIX viewProj;
	D3DXMATRIX GetViewMatrix(D3DXVECTOR3 pos, D3DXVECTOR3 rot);
	Plane planes[6];
protected:
	void UpdateComposite(float deltaTime) override;
	void RenderComposite(LPDIRECT3DDEVICE9 dev, LPD3DXEFFECT effect) override;
public:
	Camara();
	Camara(int _angle, float _width, float _height, float _minD, float _maxD);
	~Camara();
	D3DXMATRIX getViewMatrix();
	D3DXMATRIX getProjMatrix();
	D3DXMATRIX getViewProj();
};

