#include "stdafx.h"
#include "Animation.h"


Animation::Animation(std::string _nombre, float _framerate, std::vector<Frame*> _anim) : nombre(_nombre), framerate(_framerate), currentFrame(0)
{
	timer = 0;
	for (int i = 0; i < _anim.size(); i++)
	{
		anim.push_back(_anim[i]);
	}
}


Animation::~Animation()
{
	for (int i = 0; i < anim.size(); i++)
	{
		delete anim[i];
	}
}

void Animation::update(Texture* tex, float deltaTime)
{
	timer += deltaTime;
	if (timer > framerate)
	{
		currentFrame++;
		currentFrame %= anim.size();
		timer = 0;
	}
	tex->setTexSca(D3DXVECTOR2(anim[currentFrame]->getWidth() / texWidth, anim[currentFrame]->getHeight() / texHeight));
	tex->setTexMov(D3DXVECTOR2(anim[currentFrame]->getX() / texWidth, anim[currentFrame]->getY() / texHeight));
}
