#pragma once
#include "Composite.h"
#include "Vertex.h"
#include "Camara.h"

class Renderer :
	public Composite
{
	BoundingBox bb;
	Camara* c;
	VbIb* model;
	D3DXMATRIX matTrans;
public:
	Renderer(VbIb * _model, LPDIRECT3DDEVICE9 dev, Camara* _c);
	~Renderer();
	void UpdateComposite(float deltaTime) override;
	void RenderComposite(LPDIRECT3DDEVICE9 dev, LPD3DXEFFECT effect) override;
	D3DXMATRIX getMatTrans();
	void setMatTrans(D3DXMATRIX d);
};

