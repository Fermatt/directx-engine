#ifndef CAMERA_H
#define CAMERA_H
#pragma once
#include "Vertex.h"

class Camera
{
private:
	int angle;
	float width;
	float height;
	float minD;
	float maxD;
protected:
	D3DXMATRIX view;
	D3DXMATRIX proj;
	D3DXMATRIX GetViewMatrix(D3DXVECTOR3 pos, D3DXVECTOR3 rot);
public:
	Camera();
	Camera(int _angle, float _width, float _height, float _minD, float _maxD);
	~Camera();
	void Update(LPDIRECT3DDEVICE9 dev);
};

#endif