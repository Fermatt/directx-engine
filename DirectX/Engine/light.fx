struct VS_INPUT
{
	float4 position : POSITION;
	float3 normal : NORMAL;
	float2 uv : TEXCOORD;
};

struct VS_OUTPUT
{
	float4 position : SV_POSITION;
	float2 uv : TEXCOORD;
	float3 normal : TEXCOORD2;
	float4 worldPos : TEXCOORD3;
};

const int DIRECTIONAL_LIGHT = 0;
const int POINT_LIGHT = 1;
const int SPOT_LIGHT = 2;

float4x4 modelMat;
float4x4 mvpMat;
float4x4 rotMat;
float3 camPos;
int lightType;
float lightRange;
float3 lightDir;
float3 lightPos;
float4 lightCol;
float4 ambientCol;
sampler2D tex : register(s0);

float4 getDiffuseLight(float3 normal, float4 worldPos)
{
	float3 lDir = lightDir;
	float dist = 0;

	if (lightType == POINT_LIGHT || lightType == SPOT_LIGHT)
	{
		float3 diff = lightPos - worldPos;
		lDir = normalize(diff);
		dist = length(diff);
	}

	float diffuse = max(0, dot(lDir, normal));
	float falloff = 1 - dist / lightRange;

	if (lightType == SPOT_LIGHT)
	{
		float spot = dot(lDir, lightDir);
		if(spot < 0.707) falloff = 0;
	}

	//Todo esto calcula los reflejos specular
	float3 eyeDir = normalize(camPos - worldPos);
	float3 specularDir = reflect(lDir, normal);
	float specularFactor = max(0, dot(eyeDir, specularDir));
	specularFactor = pow(specularFactor, 3);
	float4 specularColor = specularFactor * float4(1, 1, 1, 1);

	return ambientCol + (lightCol * diffuse * falloff);
}

VS_OUTPUT VS(VS_INPUT vertex)
{
	VS_OUTPUT fragment;
	fragment.position = mul(vertex.position, mvpMat);
	fragment.normal = mul(vertex.normal, rotMat);
	fragment.uv = vertex.uv;
	fragment.worldPos = mul(vertex.position, modelMat);
	return fragment;
}


float4 PS(VS_OUTPUT fragment) : COLOR
{
	float4 diffuse = getDiffuseLight(fragment.normal, fragment.worldPos);
	float4 color = tex2D(tex, fragment.uv);
	return color * diffuse;
}

float4 PSSpot(VS_OUTPUT fragment) : COLOR
{
	float4 diffuse = getDiffuseLightSpot(fragment.normal, fragment.worldPos);
	float4 color = tex2D(tex, fragment.uv);
	return color * diffuse;
}

technique Efecto
{
	pass pDir
	{
		VertexShader = compile vs_2_0 VS();
		PixelShader = compile ps_2_0 PS();
	}

	pass pSpot
	{
		VertexShader = compile vs_2_0 VS();
		PixelShader = compile ps_2_0 PSSpot();
	}

	pass pPoint
	{
		VertexShader = compile vs_2_0 VS();
		PixelShader = compile ps_2_0 PS();
	};
}