#include "stdafx.h"
#include "ObjetoAnim.h"



ObjetoAnim::ObjetoAnim(D3DXVECTOR3 _mov, D3DXVECTOR3 _rot, D3DXVECTOR3 _sca) : Objeto(_mov, _rot, _sca)
{
}

ObjetoAnim::ObjetoAnim(D3DXVECTOR3 _mov, D3DXVECTOR3 _rot, D3DXVECTOR3 _sca, Objeto * _parentModel) : Objeto(_mov, _rot, _sca, _parentModel)
{
}

ObjetoAnim::~ObjetoAnim()
{
}

void ObjetoAnim::draw(LPDIRECT3DDEVICE9 dev, float deltaTime)
{
	if (model->exists)
	{
		if (tex != NULL)
		{
			animActual->update(tex, deltaTime);
			//tex->Render(dev, effect);
		}
		matTrans = GetModelMatrix(mov, rot, sca);
		if (child)
		{
			matTrans *= parentModel->getParent();
		}
		dev->SetTransform(D3DTS_WORLD, &matTrans);
		dev->SetFVF(MYFVF);
		dev->SetStreamSource(0, model->vb, 0, sizeof(Vertex));
		dev->SetIndices(model->ib);
		dev->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, model->lengthIb, 0, model->lengthIb / 3);
	}
}

void ObjetoAnim::addAnim(Animation * _anim)
{
	anim.push_back(_anim);
}

void ObjetoAnim::setAnimActual(int i)
{
	i %= anim.size();
	animActual = anim[i];
}
