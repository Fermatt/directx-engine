#include "stdafx.h"
#include "InputKey.h"


InputKey::InputKey(HINSTANCE hInstance, HWND hWnd, LPDIRECT3DDEVICE9 dev)
{
	DirectInput8Create(hInstance, DIRECTINPUT_VERSION, IID_IDirectInput8, (void**)&dip, NULL);
	dip->CreateDevice(GUID_SysKeyboard, &keyDev, NULL);
	keyDev->SetDataFormat(&c_dfDIKeyboard);
	keyDev->SetCooperativeLevel(hWnd, DISCL_FOREGROUND | DISCL_NONEXCLUSIVE);
	keyDev->Acquire();
}


InputKey::~InputKey()
{
	keyDev->Unacquire();
	keyDev->Release();
	dip->Release();
}

void InputKey::update()
{
	for (int i = 0; i < sizeof(keys); i++)
		prevKeys[i] = keys[i];
	keyDev->GetDeviceState(sizeof(keys), &keys);
}

bool InputKey::Pressed(int k)
{
	if (keys[k])
		return true;
	else
		return false;
}

bool InputKey::JustPressed(int k)
{
	if (keys[k] && !prevKeys[k])
		return true;
	else
		return false;
}

bool InputKey::JustReleased(int k)
{
	if (!keys[k] && prevKeys[k])
		return true;
	else
		return false;
}

bool InputKey::Released(int k)
{
	if (!keys[k])
		return true;
	else
		false;
}

bool InputKey::Pressed(std::string s)
{
	std::vector<int> *v = &m[s];
	for(int i = 0; i < v->size(); i++)
		if (keys[v->at(i)])
			return true;
	return false;
}

bool InputKey::JustPressed(std::string s)
{
	std::vector<int> *v = &m[s];
	for (int i = 0; i < v->size(); i++)
		if (keys[v->at(i)] && !prevKeys[v->at(i)])
			return true;
	return false;
}

bool InputKey::JustReleased(std::string s)
{
	std::vector<int> *v = &m[s];
	for (int i = 0; i < v->size(); i++)
		if (!keys[v->at(i)] && prevKeys[v->at(i)])
			return true;
	return false;
}

bool InputKey::Released(std::string s)
{
	std::vector<int> *v = &m[s];
	for (int i = 0; i < v->size(); i++)
		if (!keys[v->at(i)])
			return true;
	return false;
}

void InputKey::setMapKey(std::string s, int k)
{
	m[s].push_back(k);
}