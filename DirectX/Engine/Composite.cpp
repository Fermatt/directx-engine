#include "stdafx.h"
#include "Composite.h"
#include "Transform.h"

Composite::Composite()
{
	Add(new Transform());
}

Composite::~Composite()
{
}

void Composite::Add(Component * component)
{
	children.push_back(component);
	children.back()->SetParent(this);
	MergeChildBox();
}

void Composite::Remove(Component * component)
{
	int j = -1;
	for (int i = 0; i < children.size(); i++)
	{
		if (j >= 0)
		{
			children[j] = children[i];
			children[i] = NULL;
			j = i;
		}
		else if (children[i] == component)
		{
			delete children[i];
			children[i] = NULL;
			j = i;
		}
	}
	MergeChildBox();
}

void Composite::Update(float deltaTime)
{
	for (size_t i = 0; i < children.size(); i++)
		children[i]->Update(deltaTime);

	UpdateComposite(deltaTime);
}

void Composite::Render(LPDIRECT3DDEVICE9 dev, LPD3DXEFFECT effect)
{
	for (size_t i = 0; i < children.size(); i++)
		children[i]->Render(dev, effect);

	RenderComposite(dev, effect);
}

void Composite::UpdateComposite(float deltaTime)
{
}

void Composite::RenderComposite(LPDIRECT3DDEVICE9 dev, LPD3DXEFFECT effect)
{
}


D3DXVECTOR3 Composite::getGlobalPosition()
{
	D3DXVECTOR3 worldPos;
	if (GetParent() != NULL)
	{
		worldPos = GetParent()->getGlobalPosition();
	}
	else
	{
		worldPos = D3DXVECTOR3(0, 0, 0);
	}
	worldPos += GetComponent<Transform>()->getPos();
	return worldPos;
}

D3DXVECTOR3 Composite::getGlobalRotation()
{
	D3DXVECTOR3 worldRot;
	if (GetParent() != NULL)
	{
		worldRot = GetParent()->getGlobalRotation();
	}
	else
	{
		worldRot = D3DXVECTOR3(0, 0, 0);
	}
	worldRot += GetComponent<Transform>()->getRot();
	return worldRot;
}

void Composite::MergeChildBox()
{
	for (int i = 0; i < children.size(); i++)
	{
		if(children[i] != NULL)
			bb->Combine(children[i]->bb);
	}
	bb->Refresh();
	if(GetParent())
		GetParent()->MergeChildBox();
}