#include "stdafx.h"
#include "Transform.h"
#include "Renderer.h"


Transform::Transform()
{
	pos = D3DXVECTOR3(0, 0, 0);
	rot = D3DXVECTOR3(0, 0, 0);
	sca = D3DXVECTOR3(1, 1, 1);
	modelMat = GetModelMatrix();
	lastModelMat = modelMat;
}


Transform::~Transform()
{
}

D3DXVECTOR3 Transform::getPos()
{
	return pos;
}

D3DXVECTOR3 Transform::getRot()
{
	return rot;
}

D3DXVECTOR3 Transform::getSca()
{
	return sca;
}

void Transform::setPos(D3DXVECTOR3 _pos)
{
	pos = _pos;
}

void Transform::setRot(D3DXVECTOR3 _rot)
{
	rot = _rot;
}

void Transform::setSca(D3DXVECTOR3 _sca)
{
	sca = _sca;
}

void Transform::move(D3DXVECTOR3 offset)
{
	pos += offset;
}

void Transform::rotate(D3DXVECTOR3 offset)
{
	rot += offset;
}

void Transform::scale(D3DXVECTOR3 offset)
{
	sca += offset;
}

D3DXVECTOR3 Transform::getForward()
{
	D3DXMATRIX objRot;
	D3DXMatrixRotationYawPitchRoll(&objRot, 0, 0, rot.z);

	D3DXVECTOR3 worldForward(0, 0, 1);

	D3DXVECTOR4 objForward;
	D3DXVec3Transform(&objForward, &worldForward, &objRot);
	D3DXVECTOR3 forward(objForward.x, objForward.y, objForward.z);
	return forward;
}

D3DXVECTOR3 Transform::getRight()
{
	D3DXMATRIX objRot;
	D3DXMatrixRotationYawPitchRoll(&objRot, 0, 0, rot.z);

	D3DXVECTOR3 worldRight(1, 0, 0);

	D3DXVECTOR4 objRight;
	D3DXVec3Transform(&objRight, &worldRight, &objRot);
	D3DXVECTOR3 right(objRight.x, objRight.y, objRight.z);
	return right;
}

D3DXVECTOR3 Transform::getUp()
{
	D3DXMATRIX objRot;
	D3DXMatrixRotationYawPitchRoll(&objRot, 0, 0, rot.z);

	D3DXVECTOR3 worldUp(0, 1, 0);

	D3DXVECTOR4 objUp;
	D3DXVec3Transform(&objUp, &worldUp, &objRot);
	D3DXVECTOR3 up(objUp.x, objUp.y, objUp.z);
	return up;
}

D3DXVECTOR3 Transform::GetWorldPosition()
{
	D3DXVECTOR3 v = pos;
	if (GetParent())
		if (GetParent()->GetParent())
		{
			Transform* t = GetParent()->GetParent()->GetComponent<Transform>();
			if (t && t != this)
			{
				v += t->GetWorldPosition();
			}
		}
	return D3DXVECTOR3();
}

D3DXVECTOR3 Transform::GetWorldRotation()
{
	D3DXVECTOR3 v = rot;
	if (GetParent())
		if (GetParent()->GetParent())
		{
			Transform* t = GetParent()->GetParent()->GetComponent<Transform>();
			if (t && t != this)
			{
				v += t->GetWorldRotation();
			}
		}
	return D3DXVECTOR3();
}

D3DXMATRIX Transform::GetModelMatrix()
{
	D3DXMATRIX transMat;
	D3DXMatrixIdentity(&transMat);
	transMat._41 = pos.x;
	transMat._42 = pos.y;
	transMat._43 = pos.z;

	D3DXMATRIX scaMat;
	D3DXMatrixIdentity(&scaMat);
	scaMat._11 = sca.x;
	scaMat._22 = sca.y;
	scaMat._33 = sca.z;

	D3DXMATRIX rotXMat;
	D3DXMATRIX rotYMat;
	D3DXMATRIX rotZMat;
	D3DXMatrixRotationX(&rotXMat, D3DXToRadian(rot.x));
	D3DXMatrixRotationY(&rotYMat, D3DXToRadian(rot.y));
	D3DXMatrixRotationZ(&rotZMat, D3DXToRadian(rot.z));

	rotMat = rotXMat * rotYMat * rotZMat;
	modelMat = (scaMat * rotMat * transMat);
	return modelMat;
}

void Transform::Update(float deltaTime)
{
	if (lastModelMat != modelMat)
	{
		lastModelMat = modelMat;
		ChangeParentBB();
	}
	ChangeParentBB();
}

void Transform::Render(LPDIRECT3DDEVICE9 dev, LPD3DXEFFECT effect)
{
	if (effect != nullptr)
	{
		effect->SetMatrix("rotMat", &rotMat);
		effect->SetMatrix("modelMat", &modelMat);
	}
}

void Transform::ChangeParentBB()
{
	GetParent()->bb->Transform(modelMat);
	GetParent()->bb->Refresh();
}
