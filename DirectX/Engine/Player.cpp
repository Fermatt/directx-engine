#include "stdafx.h"
#include "Player.h"
#include "Renderer.h"
#include "Transform.h"
#include "Model.h"

void Player::UpdateComposite(float deltaTime)
{
	num += deltaTime;
	if (num > 5)
	{
		c->setAnimActual(1);
	}
}

void Player::RenderComposite(LPDIRECT3DDEVICE9 dev, LPD3DXEFFECT effect)
{
}

Player::Player(LPDIRECT3DDEVICE9 dev, Camara* cam, bool b)
{
	Renderer* r;
	Model* m = new Model(dev);
	r = new Renderer(m->getSquare(), dev, cam);
	std::vector<Frame*> frames;
	frames.push_back(new Frame(0, 0, 240, 296));
	frames.push_back(new Frame(240, 0, 240, 296));
	Texture* tex = new Texture(L"../Assets/walk.png", dev);
	std::vector<Animation*> anim;
	Animation* anim1 = new Animation("Walk", 1.0f, frames);
	std::vector<Frame*> frames2;
	frames2.push_back(new Frame(240 * 2, 0, 240, 296));
	frames2.push_back(new Frame(240 * 3, 0, 240, 296));
	Animation* anim2 = new Animation("Jump", 1.0f, frames2);
	anim.push_back(anim1);
	anim.push_back(anim2);
	c = new AnimComponent(tex, anim, 0);
	c->Add(tex);
	r->Add(c);
	Add(new Transform());
	Add(r);
	SetParent(nullptr);
}


Player::~Player()
{
}
