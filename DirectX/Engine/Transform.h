#pragma once
#include "Composite.h"

class Transform :
	public Component
{
	D3DXVECTOR3 pos;
	D3DXVECTOR3 rot;
	D3DXVECTOR3 sca;
	D3DXMATRIX lastModelMat;
public:
	D3DXMATRIX rotMat;
	D3DXMATRIX modelMat;
	Transform();
	~Transform();
	D3DXVECTOR3 getPos();
	D3DXVECTOR3 getRot();
	D3DXVECTOR3 getSca();
	void setPos(D3DXVECTOR3 _pos);
	void setRot(D3DXVECTOR3 _rot);
	void setSca(D3DXVECTOR3 _sca);
	void move(D3DXVECTOR3 offset);
	void rotate(D3DXVECTOR3 offset);
	void scale(D3DXVECTOR3 offset);
	D3DXVECTOR3 getForward();
	D3DXVECTOR3 getRight();
	D3DXVECTOR3 getUp();
	D3DXVECTOR3 GetWorldPosition();
	D3DXVECTOR3 GetWorldRotation();
	D3DXMATRIX GetModelMatrix();
	void Update(float deltaTime) override;
	void Render(LPDIRECT3DDEVICE9 dev, LPD3DXEFFECT effect) override;

	void ChangeParentBB();
};

