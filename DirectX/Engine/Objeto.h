#ifndef OBJETO_H
#define OBJETO_H
#include "Vertex.h"
#include "Texture.h"
#include <iostream>

using namespace std;

class Objeto
{

protected:
	VbIb* model;
	Objeto* parentModel;
	bool child;
	D3DXMATRIX matTrans;
	D3DXMATRIX GetModelMatrix(D3DXVECTOR3 pos, D3DXVECTOR3 rot, D3DXVECTOR3 sca);
	D3DXVECTOR3 mov;
	D3DXVECTOR3 rot;
	D3DXVECTOR3 sca;
	Texture* tex;

public:
	Objeto(D3DXVECTOR3 _mov, D3DXVECTOR3 _rot, D3DXVECTOR3 _sca);
	Objeto(D3DXVECTOR3 _mov, D3DXVECTOR3 _rot, D3DXVECTOR3 _sca, Objeto* _parentModel);
	~Objeto();
	void setModel(VbIb* _model);
	virtual void draw(LPDIRECT3DDEVICE9 dev, float deltaTime);
	D3DXMATRIX getParent();
	void setMat(D3DXMATRIX _matTrans);
	D3DXVECTOR3 getForward();
	D3DXVECTOR3 getRight();
	D3DXVECTOR3 getUp();
	D3DXVECTOR3 getRot();
	void setRot(D3DXVECTOR3 _rot);
	D3DXVECTOR3 getMov();
	void setMov(D3DXVECTOR3 _mov);
	D3DXVECTOR3 getSca();
	void setSca(D3DXVECTOR3 _sca);
	Texture* getTex();
	void setTex(Texture* _tex);
	
};
#endif