#ifndef COMPONENT_H
#define COMPONENT_H
#include "d3dx9.h"
#include "BoundingBox.h"

class Component
{
	class Composite* parent;

public:
	Component();
	~Component();
	virtual void Update(float deltaTime);
	virtual void Render(LPDIRECT3DDEVICE9 dev, LPD3DXEFFECT effect);
	void SetParent(Composite* parent);
	Composite* GetParent();

	BoundingBox* bb;
};
#endif