#ifndef OBJETO_ANIM_H
#define OBJETO_ANIM_H

#include "Objeto.h"
#include "Animation.h"
#include <vector>
class ObjetoAnim :
	public Objeto
{
private:
	std::vector<Animation*> anim;
	Animation* animActual;
public:
	ObjetoAnim(D3DXVECTOR3 _mov, D3DXVECTOR3 _rot, D3DXVECTOR3 _sca);
	ObjetoAnim(D3DXVECTOR3 _mov, D3DXVECTOR3 _rot, D3DXVECTOR3 _sca, Objeto* _parentModel);
	~ObjetoAnim();
	void draw(LPDIRECT3DDEVICE9 dev, float deltaTime);
	void addAnim(Animation* _anim);
	void setAnimActual(int i);
};

#endif