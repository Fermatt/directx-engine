#pragma once
#include "Composite.h"
#include "Animation.h"
#include <vector>

class AnimComponent :
	public Composite
{
private:
	Texture* tex;
	std::vector<Animation*> anim;
	Animation* animActual;
public:
	AnimComponent(Texture* tex, std::vector<Animation*> _anim, int _animActual);
	~AnimComponent();
	void addAnim(Animation* _anim);
	void setAnimActual(int i);
	void UpdateComposite(float deltaTime) override;
	void RenderComposite(LPDIRECT3DDEVICE9 dev, LPD3DXEFFECT effect) override;

};

