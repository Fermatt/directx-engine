#include "stdafx.h"
#include "Frame.h"


Frame::Frame(int _x, int _y, int _width, int _height) : x(_x), y(_y), width(_width), height(_height)
{

}

Frame::~Frame()
{
}

int Frame::getX()
{
	return x;
}

int Frame::getY()
{
	return y;
}

int Frame::getWidth()
{
	return width;
}

int Frame::getHeight()
{
	return height;
}
