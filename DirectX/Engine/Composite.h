#ifndef COMPOSITE_H
#define COMPOSITE_H
#include <vector>
#include "Component.h"

using namespace std;

class Composite : public Component
{
private:
	vector<Component*> children;

protected:
	virtual void UpdateComposite(float deltaTime);
	virtual void RenderComposite(LPDIRECT3DDEVICE9 dev, LPD3DXEFFECT effect);
	
	
public:
	Composite();
	~Composite();

	void MergeChildBox();
	void Add(Component* component);
	void Remove(Component* component);
	void Update(float deltaTime) override final;
	void Render(LPDIRECT3DDEVICE9 dev, LPD3DXEFFECT effect) override final;

	template<class T> T* GetComponent();
	template<class T> std::vector<T*>* GetComponents();
	template<class T> T* GetComponentInChildren();
	template<class T> T GetComponentInParent();
	template<class T> std::vector<T*>* GetComponentsInParent();

	D3DXVECTOR3 getGlobalPosition();
	D3DXVECTOR3 getGlobalRotation();


};

template<class T> T* Composite::GetComponent()
{
	for (size_t i = 0; i < children.size(); i++)
	{
		T* comp = dynamic_cast<T*>(children[i]);
		if (comp != NULL)
			return comp;
	}
	return NULL;
}

template<class T> std::vector<T*>* Composite::GetComponents()
{
	std::vector<T*>* ArrayT;
	for (size_t i = 0; i < children.size(); i++)
	{
		T* comp = dynamic_cast<T*>(children[i]);
		if (comp != NULL)
			ArrayT.push_back(comp);
	}
	return ArrayT;
}

template<class T> T* Composite::GetComponentInChildren()
{
	T* comp = dynamic_cast<T*>(this);
	if (comp) return comp;

	for (size_t i = 0; i < children.size(); i++)
	{
		Component* child = children[i];
		Composite* compositeChild = dynamic_cast<Composite*>(Child);
		if (compositeChild)
		{
			T* childComp = compositeChild->GetComponentInChildren<T>();
			if (childComp)
				return childComp;
		}
		else
		{
			T* childComp = dynamic_cast<T*>(child);
			if (childComp)
				return childComp;
		}
	}

	return NULL;
}

template<class T> T Composite::GetComponentInParent()
{
	T comp = dynamic_cast<T>(this);
	if (comp != NULL)
		return comp;
	if (GetParent() != NULL)
		return GetParent()->GetComponentInParent<T>();
	else
		return NULL;
}

template<class T> std::vector<T*>* Composite::GetComponentsInParent()
{
	std::vector<T*>* ArrayT;
	if (GetParent() != NULL)
		ArrayT = GetParent()->GetComponentsInParent<T*>();
	else
		ArrayT = new std::vector<T*>();
	T* comp = dynamic_cast<T*>(this);
	if (comp != NULL)
		ArrayT.push_back(comp);
	return ArrayT;
}

#endif