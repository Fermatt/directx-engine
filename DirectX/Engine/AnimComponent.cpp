#include "stdafx.h"
#include "AnimComponent.h"
#include "Texture.h"


AnimComponent::AnimComponent(Texture* _tex, std::vector<Animation*> _anim, int _animActual)
{
	tex = _tex;
	anim = _anim;
	animActual = anim[_animActual];
}

AnimComponent::~AnimComponent()
{
	for (int i = 0; i < anim.size(); i++)
	{
		delete anim[i];
	}
}

void AnimComponent::addAnim(Animation * _anim)
{
	anim.push_back(_anim);
}

void AnimComponent::setAnimActual(int i)
{
	animActual = anim[i];
}

void AnimComponent::UpdateComposite(float deltaTime)
{
	animActual->update(tex, deltaTime);
}

void AnimComponent::RenderComposite(LPDIRECT3DDEVICE9 dev, LPD3DXEFFECT effect)
{
}
