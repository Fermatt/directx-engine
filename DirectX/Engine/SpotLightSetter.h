#pragma once
#include "Composite.h"
#include "Transform.h"
class SpotLightSetter :
	public Composite
{
protected:
	D3DXVECTOR4 lightDir;
	D3DXVECTOR4 lightCol;
	D3DXVECTOR4 ambientCol;
	float num  = 0;
	float lightRange;
	D3DXVECTOR4 lightPos;
	int cosen;
public:
	SpotLightSetter(int coseno);
	~SpotLightSetter();
	void UpdateComposite(float deltaTime) override;
	void RenderComposite(LPDIRECT3DDEVICE9 dev, LPD3DXEFFECT effect) override;
	void setLightDir(D3DXVECTOR4 _lightDir);
	void setLightCol(D3DXVECTOR4 _lightCol);
	void setAmbientCol(D3DXVECTOR4 _ambientCol);
};

