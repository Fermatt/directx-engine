#include "stdafx.h"
#include "Camara.h"
#include "Transform.h"

Camara::Camara()
{
	angle = 60;
	width = 640;
	height = 480;
	minD = 0;
	maxD = 100;
}

Camara::Camara(int _angle, float _width, float _height, float _minD, float _maxD)
{
	angle = _angle;
	width = _width;
	height = _height;
	minD = _minD;
	maxD = _maxD;

	D3DXMatrixPerspectiveFovLH(
		&proj,
		D3DXToRadian(angle), //El angulo de apertura de la camara
		(float)width / height, //El ancho de la pantalla dividido el alto (relaci�n de aspect)
		minD, //La distancia minima del objeto para verse
		maxD); //La distancia maxima del objeto para verse
}

Camara::~Camara()
{
}

D3DXMATRIX Camara::getViewMatrix()
{
	return view;
}

D3DXMATRIX Camara::getProjMatrix()
{
	return proj;
}

D3DXMATRIX Camara::getViewProj()
{
	return viewProj;
}

D3DXMATRIX Camara::GetViewMatrix(D3DXVECTOR3 pos, D3DXVECTOR3 rot)
{
	D3DXMATRIX transMat;
	D3DXMatrixIdentity(&transMat);
	transMat._41 = -pos.x;
	transMat._42 = -pos.y;
	transMat._43 = -pos.z;

	D3DXMATRIX rotZMat;
	D3DXMatrixIdentity(&rotZMat);
	rotZMat._11 = cos(-rot.z);
	rotZMat._12 = sin(-rot.z);
	rotZMat._21 = -sin(-rot.z);
	rotZMat._22 = cos(-rot.z);

	return transMat * rotZMat;
}

void Camara::UpdateComposite(float deltaTime)
{
	view = GetViewMatrix(
		D3DXVECTOR3(0, 0, 0),
		D3DXVECTOR3(0, 0, 0));
	

	viewProj = view * proj;

	Plane rightPlane;
	rightPlane.a = viewProj._14 - viewProj._11;
	rightPlane.b = viewProj._24 - viewProj._21;
	rightPlane.c = viewProj._34 - viewProj._31;
	rightPlane.d = viewProj._44 - viewProj._41;

	Plane leftPlane;
	leftPlane.a = viewProj._14 + viewProj._11;
	leftPlane.b = viewProj._24 + viewProj._21;
	leftPlane.c = viewProj._34 + viewProj._31;
	leftPlane.d = viewProj._44 + viewProj._41;

	Plane bottomPlane;
	bottomPlane.a = viewProj._14 + viewProj._12;
	bottomPlane.b = viewProj._24 + viewProj._22;
	bottomPlane.c = viewProj._34 + viewProj._32;
	bottomPlane.d = viewProj._44 + viewProj._42;

	Plane topPlane;
	topPlane.a = viewProj._14 - viewProj._12;
	topPlane.b = viewProj._24 - viewProj._22;
	topPlane.c = viewProj._34 - viewProj._32;
	topPlane.d = viewProj._44 - viewProj._42;

	Plane nearPlane;
	nearPlane.a = viewProj._13;
	nearPlane.b = viewProj._23;
	nearPlane.c = viewProj._33;
	nearPlane.d = viewProj._43;

	Plane farPlane;
	farPlane.a = viewProj._14 - viewProj._13;
	farPlane.b = viewProj._24 - viewProj._23;
	farPlane.c = viewProj._34 - viewProj._33;
	farPlane.d = viewProj._44 - viewProj._43;

	planes[0] = rightPlane;
	planes[1] = leftPlane;
	planes[2] = bottomPlane;
	planes[3] = topPlane;
	planes[4] = nearPlane;
	planes[5] = farPlane;
}

void Camara::RenderComposite(LPDIRECT3DDEVICE9 dev, LPD3DXEFFECT effect)
{
	dev->SetTransform(D3DTS_VIEW, &view);
	dev->SetTransform(D3DTS_PROJECTION, &proj);
	Transform* t = GetComponent<Transform>();
	if (t && effect != nullptr)
		effect->SetVector("camPos", &(D3DXVECTOR4)t->getPos());
}