#ifndef VERTEX_H
#define VERTEX_H
#include "EngineApi.h"
#include "BoundingBox.h"
#include <d3d9.h>
#include <d3dx9.h> //Busca el header de directx en los path
#pragma comment (lib, "d3dx9.lib") //Incluyo la lib a mi proyecto

#define MYFVF (D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_TEX1)

struct Vertex
{
	FLOAT x, y, z;
	FLOAT nx, ny, nz;
	FLOAT tu, tv;
};

struct VbIb
{
	LPDIRECT3DVERTEXBUFFER9 vb;
	LPDIRECT3DINDEXBUFFER9 ib;
	int lengthVb, lengthIb;
	BoundingBox* bb;
	bool exists;
};
#endif