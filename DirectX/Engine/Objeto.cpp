#include "stdafx.h"
#include "Objeto.h"

//Constructor/Destructor

Objeto::Objeto(D3DXVECTOR3 _mov, D3DXVECTOR3 _rot, D3DXVECTOR3 _sca)
{
	mov = _mov;
	rot = _rot;
	sca = _sca;
	model = new VbIb();
	model->exists = false;
	child = false;
	tex = NULL;
}

Objeto::Objeto(D3DXVECTOR3 _mov, D3DXVECTOR3 _rot, D3DXVECTOR3 _sca, Objeto* _parentModel)
{
	mov = _mov;
	rot = _rot;
	sca = _sca;
	model->exists = false;
	parentModel = _parentModel;
	child = true;
	tex = NULL;
}

Objeto::~Objeto()
{
}

//Public

void Objeto::setModel(VbIb* _model)
{
	*model = *_model;
}

D3DXMATRIX Objeto::GetModelMatrix(D3DXVECTOR3 pos, D3DXVECTOR3 rot, D3DXVECTOR3 sca)
{
	D3DXMATRIX transMat;
	D3DXMatrixIdentity(&transMat);
	transMat._41 = pos.x;
	transMat._42 = pos.y;
	transMat._43 = pos.z;

	D3DXMATRIX scaMat;
	D3DXMatrixIdentity(&scaMat);
	scaMat._11 = sca.x;
	scaMat._22 = sca.y;
	scaMat._33 = sca.z;

	D3DXMATRIX rotZMat;
	D3DXMatrixIdentity(&rotZMat);
	rotZMat._11 = cos(rot.z);
	rotZMat._12 = sin(rot.z);
	rotZMat._21 = -sin(rot.z);
	rotZMat._22 = cos(rot.z);

	return (scaMat * rotZMat * transMat);
};

void Objeto::draw(LPDIRECT3DDEVICE9 dev, float deltaTime)
{
	if(model->exists)
	{
		if (tex != NULL)
		{
			//tex->Render(dev);
		}
		matTrans = GetModelMatrix(mov, rot, sca);
		if(child)
		{
			matTrans *= parentModel->getParent();
		}
		dev->SetTransform(D3DTS_WORLD, &matTrans);
		dev->SetFVF(MYFVF);
		dev->SetStreamSource(0, model->vb, 0, sizeof(Vertex));
		dev->SetIndices(model->ib);
		dev->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, model->lengthIb, 0, model->lengthIb/3);
	}
}

D3DXMATRIX Objeto::getParent()
{
	return matTrans;
}

void Objeto::setMat(D3DXMATRIX _matTrans)
{
	matTrans = _matTrans;
}

D3DXVECTOR3 Objeto::getMov()
{
	return mov;
}

void Objeto::setMov(D3DXVECTOR3 _mov)
{
	mov = _mov;
}

D3DXVECTOR3 Objeto::getRot()
{
	return rot;
}

void Objeto::setRot(D3DXVECTOR3 _rot)
{
	rot = _rot;
}

D3DXVECTOR3 Objeto::getSca()
{
	return sca;
}

void Objeto::setSca(D3DXVECTOR3 _sca)
{
	sca = _sca;
}

Texture* Objeto::getTex()
{
	return tex;
}

void Objeto::setTex(Texture* _tex)
{
	tex = _tex;
}



D3DXVECTOR3 Objeto::getForward()
{
	D3DXMATRIX objRot;
	D3DXMatrixRotationYawPitchRoll(&objRot,0,0,rot.z);

	D3DXVECTOR3 worldForward(0, 0, 1);

	D3DXVECTOR4 objForward;
	D3DXVec3Transform(&objForward, &worldForward, &objRot);
	D3DXVECTOR3 forward(objForward.x, objForward.y, objForward.z);
	return forward;
}

D3DXVECTOR3 Objeto::getRight()
{
	D3DXMATRIX objRot;
	D3DXMatrixRotationYawPitchRoll(&objRot,0,0,rot.z);

	D3DXVECTOR3 worldRight(1, 0, 0);

	D3DXVECTOR4 objRight;
	D3DXVec3Transform(&objRight, &worldRight, &objRot);
	D3DXVECTOR3 right(objRight.x, objRight.y, objRight.z);
	return right;
}

D3DXVECTOR3 Objeto::getUp()
{
	D3DXMATRIX objRot;
	D3DXMatrixRotationYawPitchRoll(&objRot,0,0,rot.z);

	D3DXVECTOR3 worldUp(0, 1, 0);

	D3DXVECTOR4 objUp;
	D3DXVec3Transform(&objUp, &worldUp, &objRot);
	D3DXVECTOR3 up(objUp.x, objUp.y, objUp.z);
	return up;
}
