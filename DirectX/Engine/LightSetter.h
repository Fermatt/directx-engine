#pragma once
#include "Composite.h"
#include "Transform.h"
class LightSetter :
	public Composite
{
protected:
	D3DXVECTOR4 lightDir;
	D3DXVECTOR4 lightCol;
	D3DXVECTOR4 ambientCol;
public:
	LightSetter();
	void UpdateComposite(float deltaTime) override;
	void RenderComposite(LPDIRECT3DDEVICE9 dev, LPD3DXEFFECT effect) override;
	void setLightDir(D3DXVECTOR4 _lightDir);
	void setLightCol(D3DXVECTOR4 _lightCol);
	void setAmbientCol(D3DXVECTOR4 _ambientCol);
	~LightSetter();
};

