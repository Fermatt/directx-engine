#ifndef ANIMATION_H
#define ANIMATION_H

#include "Frame.h"
#include "Texture.h"
#include <iostream>
#include <vector>

class Animation
{
private:
	std::string nombre;
	float framerate;
	std::vector<Frame*> anim;
	float timer;
	int currentFrame;
	float texWidth = 1440;
	float texHeight = 1480;
public:
	Animation(std::string _nombre, float _framerate, std::vector<Frame*> _anim);
	~Animation();
	void update(Texture* tex, float deltaTime);
};

#endif

