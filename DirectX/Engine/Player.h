#pragma once
#include "Composite.h"
#include "Camara.h"
#include "AnimComponent.h"

class Player :
	public Composite
{
private:
	AnimComponent* c;
	float num = 0;
protected:
	void UpdateComposite(float deltaTime) override;
	void RenderComposite(LPDIRECT3DDEVICE9 dev, LPD3DXEFFECT effect) override;
public:
	Player(LPDIRECT3DDEVICE9 dev, Camara* cam, bool b);
	~Player();
};

